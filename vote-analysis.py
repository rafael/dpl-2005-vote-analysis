#!/usr/bin/python3

# Preprocessor for the DPL 2005 vote analysis
# Copyright (C) 2024 Rafael Laboissire
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import os
import sys
import re

prog = os.path.basename(sys.argv[0])

if len(sys.argv) != 2:
    sys.stderr.write(f"Usage: {prog} year\n")
    sys.exit(1)

year = sys.argv[1]

with open("cast.csv", "w") as cast:
    with open("vote.csv", "w") as vote:
        with open(f"leader{year}_tally.txt") as fid:
            for line in fid.readlines():
                m = re.match("^V: ([-0-9]+)", line)
                if m:
                    ballot = m.group(1)
                    cast.write(f"{ballot}\n")
                    choices = list(ballot)
                    choices_sort = sorted(choices)
                    if choices_sort[0] == '-':
                        higher = str(int(choices_sort[-1]) + 1)
                        choices = [x if x != '-' else higher for x in choices]
                    vote.write(f"{','.join(choices)}\n")
