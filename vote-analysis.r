# R script for the DPL 2005 vote analysis
# Copyright (C) 2005, 2024 Rafael Laboissière
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

cast <- read.csv ("cast.csv", header = FALSE)$V1

vote.df <- read.csv ("vote.csv", header = FALSE)
vote.val <- cbind (vote.df)
vote.fa <- factanal (vote.df, factors = 3, rotation = "promax")

sink ("fa.txt")
vote.fa
sink ()

sink ("fa_p_value.txt")
cat (sprintf ("%.4f", vote.fa$PVAL))
sink ()

vote.pca <- princomp (vote.df)
change.names <- function (x) gsub("Comp.", "PC", x)
attr (vote.pca$sdev, "names") <- sapply (attr (vote.pca$sdev, "names"),
                                         change.names)
for (i in c ("loadings", "scores")) {
    dn <- attr (vote.pca [[i]], "dimnames")
    dn [[2]] <- sapply (dn [[2]], change.names)
    attr (vote.pca [[i]], "dimnames") <- dn
}

png ("pca.png", width = 400, height = 253)
par (mar = c (3, 4, 3, 0) + 0.1)
plot (vote.pca, las = 1, main = "Variances of Principal Components")
dummy <- dev.off ()

sink ("pca.txt")
summary (vote.pca)
vote.pca$loadings
sink ()

loadings <- matrix (as.numeric (vote.fa$loadings), ncol = 3)
n <- nrow (vote.df)

vote.mean <- as.numeric (colMeans (vote.df))

c1 <- matrix (0, n, 1)
c2 <- c1
c3 <- c1
sink ("projections.txt")
for (i in 1 : n) {
  coeff <- qr.solve (loadings, as.numeric (vote.val [i, ] - vote.mean))
  cat (cast [i], " ", coeff, "\n")
  c1 [i] <- coeff [1]
  c2 [i] <- coeff [2]
  c3 [i] <- coeff [3]
}
sink ()

sink ("quantiles.txt")
probs <- c (0, .25, .45, .55, .75, 1)
cat ("Quantiles for factor #1 projections:\n")
qc1 <- quantile (c1, probs = probs)
qc1
cat ("\nQuantiles for factor #2 projections:\n")
qc2 <- quantile (c2, probs = probs)
qc2
cat ("\nQuantiles for factor #3 projections:\n")
qc3 <- quantile (c3, probs = probs)
qc3
sink ()

sink ("preferences.txt")
symbols1 <- c ("--"," -"," o"," +", "++")
symbols2 <- c ("++"," +"," o"," -", "--")
cat ("ballot     REJ     AT     PS\n\n")
for (i in 1 : n) {
  cat (cast [i], "    ")
  idx <- which (c1 [i] <= qc1)
  cat (symbols1 [max (c (1, idx [1] - 1))], "    ")
  idx <- which (c2 [i] <= qc2)
  cat (symbols2 [max (c (1, idx [1] - 1))], "    ")
  idx <- which (c3 [i] <= qc3)
  cat (symbols2 [max (c (1, idx [1] - 1))], "\n")
}
sink ();

png ("fa-intervals.png", width = 370, height = 253)
par (mar = c (3, 3, 2, 0) + 0.1)
boxplot (list (c1, c2, c3), range = 0, las = 1,
         names = c ("Factor #1", "Factor #2", "Factor #3"),
         main = "Intervals of projected factors")
dummy <- dev.off ()

for (i in 1 : 3) {
  png (sprintf ("fa-%d.png", i), width = 399, height = 253)
  par (mar = c (3, 4, 2, 0) + 0.1)
  a <- (i - 1) * 7
  ld <- as.numeric(vote.fa$loadings)[(a + 1) : (a + 7)]
  barplot (ld, col = 'green4', axes = TRUE, ylab ="loadings", las = 1,
           main = sprintf ("Factor #%d", as.integer (i)),
           ylim = c (-0.5, 1.0), width = 0.8, space = 0.25)
  box ()
  axis (1, seq (0, 6) + 0.6, c("JW", "MG", "BR", "AT", "AL", "AS", "NA"))
  lines (c (-1, 8), c (0, 0))
  dummy <- dev.off ()
}

