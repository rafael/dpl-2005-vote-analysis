#!/usr/bin/python3

# Automatic generation of README file
# Copyright (C) 2024 Rafael Laboissière
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from jinja2 import Environment, FileSystemLoader

environment = Environment(loader=FileSystemLoader("."))
template = environment.get_template("README.md.in")

context = {}

for f in ["pca", "fa", "quantiles", "fa_p_value"]:
    with open(f"{f}.txt", "r") as fid:
        context[f] = f"{fid.read()}"

with open("README.md", "w") as fid:
    fid.write(template.render(context))
