# Makefile for the DPL 2005 vote analysis
# Copyright (C) 2005, 2024 Rafael Laboissière
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

PNGFILES = fa-1.png fa-2.png fa-3.png pca.png fa-intervals.png
TXTFILES = pca.txt fa.txt quantiles.txt projections.txt preferences.txt	\
           fa_p_value.txt
CSVFILES = vote.csv cast.csv

all: README.md $(PNGFILES)

README.md: README.md.in $(TXTFILES)
	./gen-readme.py

$(TXTFILES) $(PNGFILES): vote-analysis.r $(CSVFILES)
	Rscript vote-analysis.r

$(CSVFILES): vote-analysis.py leader2005_tally.txt
	./vote-analysis.py 2005

display: $(PNGFILES)
	display vid:*.png

clean:
	rm -rf README.md $(PNGFILES) $(CSVFILES) $(TXTFILES)

.PHONY: all clean display
